from fastapi.testclient import TestClient

from main import app

client = TestClient(app)


def test_read_main():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg": "Hello World"}


def test_read_item():
    item_id = 1
    q = "test"
    response = client.get(f"/items/{item_id}?q={q}")
    assert response.status_code == 200
    assert response.json() == {"item_id": item_id, "q": q}
